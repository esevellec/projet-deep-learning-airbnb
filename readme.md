# Projet deep learning AirBnb

## Préparation des Données

Les données brutes utilisées sont stockées dans le fichier `airbnb-listings.csv`. Elles sont ensuites traités par le script `creationData.py` pour ne garder que les colonnes pertinentes et supprimer les lignes incomplètes. Ce script sépare donc les données obtenues en deux fichiers `AirBnbTrain.csv` contenant 80% des données obtenues et `AirBnbTest.csv` qui contient les 20 autres pourcents. Le script `traitement.py` permet ensuite de transformer les données qui ne sont pas des nombres en classes et à transformer les prix en classes de prix. Il en résulte les deux fichiers de données finaux utilisés `AirBnbTrainTraite.csv` et `AirBnbTestTraite.csv`.

## Entrainement

Le réseau de neurone est ensuite entrainé par le script python `train.py`. Ce script indique en fin d'exécution la précision estimée du réseau de neurone à la fin de l'entrainement.
