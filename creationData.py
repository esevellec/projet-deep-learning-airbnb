import pandas as pd
from numpy.random import RandomState

# import du fichier csv de données
# on découpe le data en chunks pour que le traitement
file = pd.read_csv("airbnb-listings.csv",sep=';', error_bad_lines=False, engine='python', chunksize=1000)

# Création des headers
with open('AirBnbTrain.csv', 'a') as fileTrain:
    fileTrain.write('ID,"Host Since","City","Host Response Rate","Host Response Time","Host Acceptance Rate","Country Code","Accommodates","Bathrooms","Bedrooms","Beds","Property Type","Room Type","Guests Included","Availability 365","Number of Reviews","Review Scores Rating","Review Scores Accuracy","Review Scores Cleanliness", "Review Scores Checkin","Review Scores Communication","Review Scores Location","Review Scores Value","Reviews per Month","Price"\n')
with open('AirBnbTest.csv', 'a') as fileTest:
    fileTest.write('ID,"Host Since","City","Host Response Rate","Host Response Time","Host Acceptance Rate","Country Code","Accommodates","Bathrooms","Bedrooms","Beds","Property Type","Room Type","Guests Included","Availability 365","Number of Reviews","Review Scores Rating","Review Scores Accuracy","Review Scores Cleanliness", "Review Scores Checkin","Review Scores Communication","Review Scores Location","Review Scores Value","Reviews per Month","Price"\n')

# compteur
i = 0
for chunk in file :
     # on affiche le compteur de chunks
     print(i)
     i += 1

     # on sélectionne les colonnes qui nous semblent influencer le prix et le prix
     data = chunk[["Host Since","City", "Host Response Rate", "Host Response Time", "Host Acceptance Rate", "Country Code","Accommodates", "Bathrooms", "Bedrooms", "Beds", "Property Type","Room Type","Guests Included", "Availability 365","Number of Reviews", "Review Scores Rating","Review Scores Accuracy", "Review Scores Cleanliness", "Review Scores Checkin", "Review Scores Communication", "Review Scores Location", "Review Scores Value","Reviews per Month", "Price"]]

     #on enlève les données non renseignées
     data2 = data.dropna()

     #on shuffle les données en 80% train et 20% test
     rng = RandomState()
     train = data2.sample(frac=0.8, random_state=rng)
     test = data2.loc[~data2.index.isin(train.index)]

     #on ajoute les données à la suite des fichiers train et test
     train.to_csv("AirBnbTrain.csv",mode='a', header=False)
     test.to_csv("AirBnbTest.csv",  mode='a', header=False)