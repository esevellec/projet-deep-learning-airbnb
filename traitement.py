import pandas as pd
from sklearn.preprocessing import LabelEncoder

#on ouvre les fichier train et test
train = pd.read_csv("AirBnbTrain.csv")
test = pd.read_csv("AirBnbTest.csv")

# on regarde le data qui est qualitatif
print(train.select_dtypes(include=['object']).columns)

#on supprime les ID
train.drop("ID", axis = 1, inplace = True)
test.drop("ID", axis = 1, inplace = True)

#on ne garde que l'année d'ajout de l'hôte
train['Host Since'] = train['Host Since'].transform(lambda x : int(x.split('-')[0]))
test['Host Since'] = test['Host Since'].transform(lambda x : int(x.split('-')[0]))

# création d'un encoder
encoder = LabelEncoder()

#on encode le nom des villes
encoder.fit(train['City'])
train['City'] = encoder.transform(train['City'])
encoder.fit(test['City'])
test['City'] = encoder.transform(test['City'])

#on encode le temps de réponse
encoder.fit(train['Host Response Time'])
train['Host Response Time'] = encoder.transform(train['Host Response Time'])
encoder.fit(test['Host Response Time'])
test['Host Response Time'] = encoder.transform(test['Host Response Time'])

#on enlève le pourcentage du taux d'acceptance
train['Host Acceptance Rate'] = train['Host Acceptance Rate'].transform(lambda x : int(x.split('%')[0]))
test['Host Acceptance Rate'] = test['Host Acceptance Rate'].transform(lambda x : int(x.split('%')[0]))

#on encode le pays
encoder.fit(train['Country Code'])
train['Country Code'] = encoder.transform(train['Country Code'])
encoder.fit(test['Country Code'])
test['Country Code'] = encoder.transform(test['Country Code'])

#on encode le type de propriété
encoder.fit(train['Property Type'])
train['Property Type'] = encoder.transform(train['Property Type'])
encoder.fit(test['Property Type'])
test['Property Type'] = encoder.transform(test['Property Type'])

#on encode le type de chambre
encoder.fit(train['Room Type'])
train['Room Type'] = encoder.transform(train['Room Type'])
encoder.fit(test['Room Type'])
test['Room Type'] = encoder.transform(test['Room Type'])


#on trie les prix par tranches de 20
def tri(x):
    if (x >= 450): 
        return 10
    else: 
        return x//50

train['Price'] = train['Price'].transform(lambda x : tri(x))
test['Price'] = test['Price'].transform(lambda x : tri(x))

#on écrit les fichiers csv
train.to_csv("AirBnbTrainTraite.csv")
test.to_csv("AirBnbTestTraite.csv")

