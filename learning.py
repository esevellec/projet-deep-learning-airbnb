import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib
import matplotlib.pyplot as plt
import warnings
#import xgboost as xgb
#import lightgbm as lgb
from scipy.stats import skew
from scipy import stats
from scipy.stats.stats import pearsonr
from scipy.stats import norm
from collections import Counter
from sklearn.linear_model import LinearRegression,LassoCV, Ridge, LassoLarsCV,ElasticNetCV
from sklearn.model_selection import GridSearchCV, cross_val_score, learning_curve
from sklearn.ensemble import RandomForestRegressor, AdaBoostRegressor, ExtraTreesRegressor, GradientBoostingRegressor
from sklearn.preprocessing import StandardScaler, Normalizer, RobustScaler
warnings.filterwarnings('ignore')
sns.set(style='white', context='notebook', palette='deep')

# Load train and Test set
train = pd.read_csv("/Volumes/KESU/TP/MachineLearning/AirBnbTrain.csv")
test = pd.read_csv("/Volumes/KESU/TP/MachineLearning/AirBnbTest.csv")

train.drop("ID", axis = 1, inplace = True)
test.drop("ID", axis = 1, inplace = True)

print(train.head())

print(train['Price'].describe())

sns.distplot(train['Price'] , fit=norm);

# Get the fitted parameters used by the function
# (mu, sigma) = norm.fit(train['Price'])
# print( '\n mu = {:.2f} and sigma = {:.2f}\n'.format(mu, sigma))
# plt.legend(['Normal dist. ($\mu=$ {:.2f} and $\sigma=$ {:.2f} )'.format(mu, sigma)],
#             loc='best')
# plt.ylabel('Frequency')
# plt.title('Price distribution')
#
# fig = plt.figure()
# res = stats.probplot(train['Price'], plot=plt)
# plt.show()
#
# print("Skewness: %f" % train['Price'].skew())
# print("Kurtosis: %f" % train['Price'].kurt())


# # Checking Categorical Data
print(train.select_dtypes(include=['object']).columns)
#
# # Checking Numerical Data
print(train.select_dtypes(include=['int64','float64']).columns)
#
# cat = len(train.select_dtypes(include=['object']).columns)
# num = len(train.select_dtypes(include=['int64','float64']).columns)
# print('Total Features: ', cat, 'categorical', '+',
#       num, 'numerical', '=', cat+num, 'features')

# Correlation Matrix Heatmap
corrmat = train.corr()
f, ax = plt.subplots(figsize=(12, 9))
sns.heatmap(corrmat, vmax=.8, square=True)
plt.show()

# Top 10 Heatmap
k = 10 #number of variables for heatmap
cols = corrmat.nlargest(k, 'Price')['Price'].index
cm = np.corrcoef(train[cols].values.T)
sns.set(font_scale=1.25)
hm = sns.heatmap(cm, cbar=True, annot=True, square=True, fmt='.2f', annot_kws={'size': 10}, yticklabels=cols.values, xticklabels=cols.values)
plt.show()

most_corr = pd.DataFrame(cols)
most_corr.columns = ['Most Correlated Features']
print(most_corr)


